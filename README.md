# Usage
Check the file for a more specific explanation. Here will be a few demonstrations of the lib.
Each example, and all use of the lib starts with these lines (implied to be there before each example):

```c
barg_alloc(a, 5); // make array "a" that can contain 5 args
barg_init(a, argc, argv); // populate array "a" with argv
```

#### 1. check for a flag and do something if found:
```c
unsigned int k = barg_find(a, "-x"); // barg_find returns the index of the flag if found

if (k) // it returns 0 if not found so it can be put in an if by itself
{
	// printf("Found -x");
	// or do something else
}
```
This can be shortened to the following as we don't need to store the return value.
```c
if (barg_find(a, "-x")) // when found it returns more than 0 and activates the if
{
	// do
}
```
#### 2. see if the flag exists and has a value associated with it, only then do something:
```c
unsigned int k = barg_find(a, "-file");

if (k && a[k].val) // check if found and only then check if there is a value at the index
{
	// printf("Found %s with %s", a[k].flag, a[k].val); // accessing the flag and val is done in this way
	// or do anything else
}
```
In this example we used the return value to access the two fields, so it is better written as it is.

#### 3. associate a flag with another so it doesn't work without it (nesting):
```c
if (barg_find(a, "-x")) // as before, if you don't care about the value of a flag you can write it so
{
	k = barg_find(a, "--audio-quality"); // obviously, only searches if -x was found in the argument list before
	if (k && a[k].val)
	{
		// printf("got both and a val");
	}
}
```
**Note** that this lib doesn't really care about order as "prog -x --audio-quality 2" and "prog -audio-quality 2 -x" will have the same result, so the nesting in this example, as said, doesn't enforce order.
