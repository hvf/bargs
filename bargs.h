/* definitions and declarations */

// consists of a flag and associated value
typedef struct {
	char* flag;
	char* val;
} barg;

// global values for the functions to use
unsigned short int blimit = 0;
unsigned short int bcount = 0;

// organizes the arguments into an array of the barg struct
// the arguments are 1-indexed for the barg_find function to
// work with if statemnts better and so b[0] is empty
void barg_init(barg b[], int argc, char* argv[]);

// compares the flag with each one in the initialized array
// returns the index of the matching flag
// but returns 0 if not found (to work nicely with if statements)
unsigned int barg_find(barg b[], char* flag);

// this macro makes an array of barg with a length and name you want
// barg_alloc[10] // can store 10 flags and their values
#define barg_alloc(NAME, LIMIT) \
	blimit = LIMIT; \
	barg NAME[blimit]; \
// 1 set the limit to the given one
// 2 make an array with given name and limit

/* function bodies */

void barg_init(barg b[], int argc, char* argv[]) {
	short int i=1, index=1; // start from arg 1
	short int gotflag=0;

	while (i < argc && bcount <= blimit) {
		// if got flag and not last element
		if (argv[i][0] == '-' ) {
			if (bcount < blimit) { // condition helps fix a bug. apologies
				// if got flag before too then make val null for easy if checking
				if (gotflag == 1) b[index++].val = NULL;
				// with each flag add one to global arg count
				bcount++;
				// add flag and say got flag
				gotflag=1;
				b[index].flag = argv[i];
			}
		// else if got val
		} else {
			// if got flag accept val
			if (gotflag == 1) b[index++].val = argv[i];
			// say got no flag
			gotflag=0;
		}
		i++; // next arg
	}
}

unsigned int barg_find(barg b[], char* flag) {
	short int match = 0;

	for (int i=1; i <= bcount; i++) {
		for (int j=0; flag[j] != '\0'; j++) {
			// if matches say so
			if (b[i].flag[j] == flag[j]) match=1;
			// else say no and next word
			else { match=0; break; }
		}

 		// return this word matches return index
		if (match == 1) return i;
	}

	// if nothing matches return 0
	if (match == 0) return 0;
}
